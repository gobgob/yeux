/**---------------------------------------- **/
/**-------------   CORE   ----------- **/
/**-------------------------------------- **/

/**
* Prepare layer buffer before execute()
* @param int layer
* @param int offsetX
* @param int mode
**/
void prepare(int layer, int offset, int mode)
{
    setMode(mode);
    offsetX(offset, layer);
}

/**
* Set Mode for second eye
* @param int mode
**/
void setMode(int mode)
{
    g_mode = mode;
}

/**
* Offset X - Offset layer in a buffer layer
* @param int offset
* @param int layer 
**/
void offsetX(int offset, int layer)
{
	if (g_mode == DOUBLE)
	{
		for(int i =0;i<8;i++)
		{
			// Clone (layer)
			g_layer[layer][i][RED]   |= ( g_layer[layer][i][RED]   << 16);
			g_layer[layer][i][GREEN] |= ( g_layer[layer][i][GREEN] << 16);
			g_layer[layer][i][BLUE]  |= ( g_layer[layer][i][BLUE]  << 16);
		}
		setMode(DIFF);
	}
	cleanBuffer(layer);
	offset = offset -16; // Correction of 0 axis
	for(int i =0;i<8;i++)
	{
		if(offset < 0)
		{
			g_buffer_layer[layer][i][RED] = ( g_layer[layer][i][RED] << -offset);
			g_buffer_layer[layer][i][GREEN] = ( g_layer[layer][i][GREEN] << -offset);
			g_buffer_layer[layer][i][BLUE] = ( g_layer[layer][i][BLUE] << -offset);
		}
		else
		{
			g_buffer_layer[layer][i][RED] = ( g_layer[layer][i][RED] >> offset);
			g_buffer_layer[layer][i][GREEN] = ( g_layer[layer][i][GREEN] >> offset);
			g_buffer_layer[layer][i][BLUE] = ( g_layer[layer][i][BLUE] >> offset);
		}
		switch (g_mode)
		{
			case CLONE:
				// Clean second eye
				g_buffer_layer[layer][i][RED]   &= 0xffff0000;
				g_buffer_layer[layer][i][GREEN] &= 0xffff0000;
				g_buffer_layer[layer][i][BLUE]  &= 0xffff0000;
				
				// Clone buffer of first eye in the second eye
				g_buffer_layer[layer][i][RED]   |= ( g_buffer_layer[layer][i][RED]   >> 16);
				g_buffer_layer[layer][i][GREEN] |= ( g_buffer_layer[layer][i][GREEN] >> 16);
				g_buffer_layer[layer][i][BLUE]  |= ( g_buffer_layer[layer][i][BLUE]  >> 16);	
				break;
				
			case SYM:
				// Clean second eye
				g_buffer_layer[layer][i][RED]   &= 0xffff0000;
				g_buffer_layer[layer][i][GREEN] &= 0xffff0000;
				g_buffer_layer[layer][i][BLUE]  &= 0xffff0000;
				
				// Symmetric
				g_buffer_layer[layer][i][RED]   |= symmetry(g_buffer_layer[layer][i][RED] >> 16); //i = row
				g_buffer_layer[layer][i][GREEN] |= symmetry(g_buffer_layer[layer][i][GREEN] >> 16);
				g_buffer_layer[layer][i][BLUE]  |= symmetry(g_buffer_layer[layer][i][BLUE]  >> 16);
				break;
		}
	} 
}

/**
* Offset Y - Offset Y buffer layer directly
* @param int offsetY
* @param int layer
**/
void offsetY(int offset, int layer)
{
    unsigned long buffer[8][3];
    for(int i = 0;i < 8; i++)
    {
        buffer[i][RED]     = g_buffer_layer[layer][i][RED]  ;
        buffer[i][BLUE]   = g_buffer_layer[layer][i][BLUE] ;
        buffer[i][GREEN] = g_buffer_layer[layer][i][GREEN];
    }
    
    for(int i = 0;i < 8; i++)
    {
        if (i+offset >= 0 &&i+offset < 8)
        {
            g_buffer_layer[layer][i][RED]      =   buffer[i+offset][RED];
            g_buffer_layer[layer][i][GREEN]    =   buffer[i+offset][GREEN];
            g_buffer_layer[layer][i][BLUE]     =   buffer[i+offset][BLUE];  
        }
        else
        {
            g_buffer_layer[layer][i][RED]      =   0;
            g_buffer_layer[layer][i][GREEN]    =   0;
            g_buffer_layer[layer][i][BLUE]     =   0;
        }
    }
}

/**
* Clean layer
* @param int layer
**/
void cleanLayer(int layer)
{
    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 3; j++)
            g_layer[layer][i][j] = 0;
}

/**
* Clean all layers
**/
void cleanAllLayers()
{
   for (int layer=0; layer < NB_LAYER; layer++)
		cleanLayer(layer);
}

/**
* Clean buffer of selected layer
* @param int layer
**/
void cleanBuffer (int layer)
{
	for(int i =0;i<8;i++)
	{
		g_buffer_layer[layer][i][RED] = 0;
		g_buffer_layer[layer][i][GREEN] = 0;
		g_buffer_layer[layer][i][BLUE] = 0;
	}
}

/**
* Clean all Buffers
**/
void cleanAllBuffers ()
{
	for (int layer=0; layer < NB_LAYER; layer++)
		cleanBuffer(layer);
}

/**
* Byte symmetry
* 0100 0000 -> 0000 0010
* @param unsigned short sprite
**/
unsigned short symmetry(unsigned short sprite)
{
	unsigned short sprite_return = 0;
	for (int i = 0 ; i < 16 ; i++)
	{
		if ( 1 << 15-i == (sprite & 1 << 15-i )) // Magic appends here !
			sprite_return |= 1 << i;
	}
	return sprite_return;
}

/**
* Refresh Sprite for display
* Blend buffers layers 1 & 2
**/
void execute()
{
    // Set g_sprite with currents layers buffers
	for (int row = 0; row < 8; row++)
	{
        // Clean color Byte for each row
        unsigned long redByte = 0;
        unsigned long blueByte = 0;
        unsigned long greenByte = 0;
        
		for (int i = 0; i < 8; i++)
		{
			// Red
            for (int layer=0; layer < NB_LAYER; layer++)
                redByte |= g_buffer_layer[layer][row][RED];
			
			g_sprite[row*4+1][7-i] = ~B00000000;

			if (redByte&(1UL << i+24)) // B0000000i 00000000 00000000 00000000 (Matrix 1)
				g_sprite[row*4+1][7-i] &= ~B00001000;
			if (redByte&(1UL << i+16)) // B00000000 0000000i 00000000 00000000 (Matrix 2)
				g_sprite[row*4+1][7-i] &= ~B00000100;
			if (redByte&(1UL << i+8))   // B00000000 00000000 0000000i 00000000 (Matrix 3)
				g_sprite[row*4+1][7-i] &= ~B00000010;	
			if (redByte&(1UL << i))      // B00000000 00000000 00000000 0000000i (Matrix 4)
				g_sprite[row*4+1][7-i] &= ~B00000001;
			
			// Blue
            for (int layer=0; layer < NB_LAYER; layer++)
                blueByte |= g_buffer_layer[layer][row][BLUE]; 
			
			g_sprite[row*4+2][7-i] = ~B00000000;
			
			if (blueByte&(1UL << i+24)) // B0000000i 00000000 00000000 00000000 (Matrix 1)
				g_sprite[row*4+2][7-i] &= ~B00001000;	
			if (blueByte&(1UL << i+16)) // B00000000 0000000i 00000000 00000000 (Matrix 2)
				g_sprite[row*4+2][7-i] &= ~B00000100;	
			if (blueByte&(1UL << i+8))   // B00000000 00000000 0000000i 00000000 (Matrix 3)
				g_sprite[row*4+2][7-i] &= ~B00000010;
			if (blueByte&(1UL << i))      // B00000000 00000000 00000000 0000000i (Matrix 4)
				g_sprite[row*4+2][7-i] &= ~B00000001;
				
			// Green
            for (int layer=0; layer < NB_LAYER; layer++)
                greenByte |= g_buffer_layer[layer][row][GREEN];
			
			g_sprite[row*4+3][i] = ~B00000000;
			
			if (greenByte&(1UL << i+24)) // B0000000i 00000000 00000000 00000000 (Matrix 1)
				g_sprite[row*4+3][i] &= ~B00001000;
			if (greenByte&(1UL << i+16)) // B00000000 0000000i 00000000 00000000 (Matrix 2)
				g_sprite[row*4+3][i] &= ~B00000100;
			if (greenByte&(1UL << i+8))   // B00000000 00000000 0000000i 00000000 (Matrix 3)
				g_sprite[row*4+3][i] &= ~B00000010;
			if (greenByte&(1UL << i))      // B00000000 00000000 00000000 0000000i (Matrix 4)
				g_sprite[row*4+3][i] &= ~B00000001; 
				
		}
	}
    
    // Send g_sprite for display
    matrix();
}

/**
* Set led into a layer
* @param int layer
* @param int x
* @param int y
* @param int color (same number as API colors)
**/
void setLed(int layer, int x, int y, int color)
{
    switch (color)
    {
        case 0:
            // Black
            g_layer[layer][y][RED]      &= 0 << x;
            g_layer[layer][y][GREEN] &= 0 << x;
            g_layer[layer][y][BLUE]    &= 0 << x;
            break;
        case 1:
            // Blue
            g_layer[layer][y][RED]      &= 0 << x;
            g_layer[layer][y][GREEN] &= 0 << x;
            g_layer[layer][y][BLUE]    &= 1 << x;
            break;
        case 2:
            // Green
            g_layer[layer][y][RED]      &= 0 << x;
            g_layer[layer][y][GREEN] &= 1 << x;
            g_layer[layer][y][BLUE]    &= 0 << x;
            break;
        case 3:
            // Cyan
            g_layer[layer][y][RED]      &= 0  << x;
            g_layer[layer][y][GREEN] &= 1 << x;
            g_layer[layer][y][BLUE]    &= 1 << x;
            break;
        case 4:
            // Red
            g_layer[layer][y][RED]      &= 1 << x;
            g_layer[layer][y][GREEN] &= 0 << x;
            g_layer[layer][y][BLUE]    &= 0 << x;
            break;
        case 5:
            // Purple
            g_layer[layer][y][RED]      &= 1 << x;
            g_layer[layer][y][GREEN] &= 0 << x;
            g_layer[layer][y][BLUE]    &= 1 << x;
            break;
        case 6:
            // Yellow
            g_layer[layer][y][RED]      &= 1 << x;
            g_layer[layer][y][GREEN] &= 1 << x;
            g_layer[layer][y][BLUE]    &= 0 << x;
            break;
        case 7:
            // White
            g_layer[layer][y][RED]      &= 1 << x;
            g_layer[layer][y][GREEN] &= 1 << x;
            g_layer[layer][y][BLUE]    &= 1 << x;
            break;
    }
}

/**
* Blink all leds in a color
*
* Be carefull, you need detach interrupt for this function
* @param bool red
* @param bool green
* @param bool blue
* @param int delayms
**/
void blink(bool red, bool green, bool blue, int delayms)
{
    // color = B00001111 <- B0000(Green)(Blue)(Red)(Row)
    unsigned short color = B0000000 | red << 1 | blue << 2 | green << 3;
    
    for (int j = 0; j < 4; j++)
    {
        for (int i = 0; i < 8; i++)
        {
            if (color & 1 << j)
                PORTC = B00000000;
            else
                PORTC = B00001111;
            clock();
        }
    }
    latch();
    
    delay(delayms);
}

/**
* Display g_sprite on the eyes
**/
void matrix()
{
	digitalWrite(6, HIGH); // For oscillo debug
    for (int i = 0; i<32 ; i++)
    {
		for (int j = 0; j<8 ; j++)
		{
			PORTC = g_sprite[i][j];
			clock();
		}
		if ((i+1)%4 == 0) latch();
    }
	matrixNull(); // Clean display before
	digitalWrite(6, LOW); // For oscillo debug
}

/**
* Clean display
**/
void matrixNull()
{
	for (int i = 0; i<32 ; i++)
    {
        PORTC = B00000000;
        clock();
    }
    latch();
}

/**
* Init row scanning process (set g_sprite row mask)
* Only one call for this function is needed
**/
void initRow()
{
	for (int row = 0; row < 8; row++)
	{
		for (int i = 0; i < 8; i++)
		{
			if (row == i)
				g_sprite[row*4][i] = B00001111;
			else
				g_sprite[row*4][i] = B00000000;
		}
	}
}

void latch()
{
    digitalWrite(LATCH,HIGH);
    digitalWrite(LATCH,LOW);
}

void clock()
{
    digitalWrite(CLOCK,HIGH);
    digitalWrite(CLOCK,LOW);
}