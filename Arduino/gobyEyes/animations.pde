/**---------------------------------------- **/
/**-------          ANIMATIONS   --------- **/
/**-------------------------------------- **/

void animation(int anim)
{
    int lap = 0;
    int mem[3];
    switch (anim)
    {
    case SPACE_IN_VADER :
        for(int color =0;color<6;color++)
        {
            spaceInVader(color); // Set spaceinvader into buffer_layer01
            for (int offset = 0; offset <3; offset++)
            {
                prepare(0, offset, CLONE); // layer, offsetX, mode
                execute();
                delay(50);
            }
            for (int offset = 2; offset >-4; offset--)
            {
                prepare(0, offset, CLONE); // layer, offsetX, mode
                execute();
                delay(50);
            }
            for (int offset = -3; offset <1; offset++)
            {
                prepare(0, offset, CLONE); // layer, offsetX, mode
                execute();
                delay(50);
            }
        }
        break;

    case PACMAN:
        // scene 01
        lap = 0;
        for (int offset = -10; offset < 45; offset++)
        {
            // Pacman - Layer01
            pacman(abs(offset)%2); // Put pacman into layer01 (open and close for eating)
            prepare(0, offset, DIFF);
            pacmanObject(1); // Put pacman object into layer02 (bullet)
            
            // Bullets - Layer02
            if (offset%4 == 0 && offset > -5) lap++;
            prepare(1, 14+4*lap, DOUBLE); // layer02 -> buffer_layer02

            // Ghost - Layer03
            if (offset > 6)
            {
                pacmanObject(2);
                prepare(2, offset-12, DIFF);
            }
            execute();
            delay(100);
        }
        break; 
        
    case STROB:
        Timer1.detachInterrupt();
        for (int i = 0; i < 10; i++)
        {
            blink(1,0,0,100); // White during 0,1sc
            blink(0,0,0,50);
        }
        Timer1.attachInterrupt(matrix);
        break;
        
    case K2000:
        figure(K2000);
        for (int i = 0; i < 16; i++) // Aller
        {
            prepare(0,i,SYM);
            execute();
            delay(65-i*3);
        }
        for (int i = 16; i > 0 ; i--) // Relap
        {
            prepare(0,i,SYM);
            execute();
            delay(65-i*3);
        }
        break;
    case GIFT:
        figure(GIFT);
        for (int i = 10; i > 0; i--)
        {
            prepare(0, 4, CLONE);
            offsetY(i, 0);
            execute();
            delay(30);
        }
        prepare(0, 4, CLONE);
        execute();
        delay(1000);
        for (int i = 4; i > -10; i-- )
        {
            prepare(0, i, SYM);
            execute();
            delay(30);
        }
        break;
    case POLICE:
        Timer1.detachInterrupt();
        for (int i = 0; i < 10; i++)
        {
            blink(1,0,0,200);
            blink(0,0,1,200);
        }
        Timer1.attachInterrupt(matrix);
        break;
    case COUNTDOWN:
        for (int i = 10; i > 0; i --)
        {
            countDown(i);
            prepare(1,0,CLONE);
            execute();
            delay(1000);
        }
        for (int i=0; i < 10; i++)
        {
            if (i%2 == 0)
                countDown(0);
            else
                countDown(11);
            prepare(1,0,CLONE);
            execute();
            delay(100);
        }
    case JACKPOT:
        for ( lap = 1; lap < 6; lap ++)
        {
            // Prepare 3 layers
            for (int i = 0; i < 3; i++)
            {
                jackpot(mem[i] = random(1,4), i);
            }
            for (int offset = 2; offset > -2; offset--)
            {
                prepare(0, 0 , CLONE); // layer, offset, mode
                prepare(1, 5 , CLONE); // layer, offset, mode
                prepare(2, 10, CLONE); // layer, offset, mode
                
                offsetY(offset*(3/lap)  , 0);
                offsetY(offset*(-2/lap) , 1);
                offsetY(offset*(3/lap), 2);

                execute();
                delay(30);
                if (lap == 5)
                    delay(500);
            }
        }
        cleanAllBuffers();
        if (mem[0] == mem[1] && mem[0] == mem[2]) // Win animation
        {
            figure(DOLLAR);
            for (int i = 0; i < 5; i++)
            {
                prepare(0, 3, CLONE);
                execute();
                delay(200);
                Timer1.detachInterrupt();
                blink(0,0,0,200);
                Timer1.attachInterrupt(matrix);
            }
        }
        break;
    case LOVE:
        for (lap = 0; lap < 10; lap ++)
        {
            if ( lap%2 == 0)
                figure(HEART);
            else
                figure(SHEART);
            prepare(0, 3, SYM);
            execute();
            delay(100);
            if (lap%2 == 0)
                delay(200);
        }
    }
    
    // Clean all buffers
    cleanAllBuffers();
    animNum = 0;
}

void standby()
{
    static bool lap = false;
    static int  offset = 0;
    static bool forward = true;
    
    figure(EYE);
    prepare(0, offset, CLONE);
    execute();
    delay(10);
    
    if (lap)
    {
        if (forward)
        offset++;
        else
        offset--;
    }    
    
    if (offset == 8)
    forward = false;
    if (offset == 0)
    forward = true;
    
    lap = !lap;
    cleanAllBuffers();
}