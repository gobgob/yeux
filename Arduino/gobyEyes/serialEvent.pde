/*
SerialEvent occurs whenever a new data comes in the
hardware serial RX.  This routine is run between each
time loop() runs, so using delay inside loop can delay
response.  Multiple bytes of data may be available.

NB : \r or \n is require for send a command

* COMMANDS *
** Animation **
    A01 -> Animation 1
** Servo **
    SR001 -> Servo Right 1deg
    SL180 -> Servo Left 180deg
** Offset **
    O10,1 -> Offset 10 to layer1
** Refresh **
    R -> Execute()
** Mode **
    M1 -> Mode auto
    M2 -> Mode semi-auto
    M3 -> Mode manual
** Version **
    V -> version
** BlackOut **
    B -> BlackOut
** Set led **
    L(layer),(x),(y),(color)
*/
void serialEvent() 
{
    while (Serial.available()) 
    {
        char inChar = (char) Serial.read();
        
        switch (inChar)
        {
            case '0'...'9':
                serialInput[serialPrompt] = inChar-'0';
                serialPrompt++;
                break;
            case '\n':
                serialExecute();
                break;
            default:
                serialInput[serialPrompt] = inChar;
                serialPrompt++;
                break;
        }
    }
}

void serialExecute()
{
	Timer1.detachInterrupt();
    int pos = 0;
    int j = 0;
    int command[4];
    int minus = 1;
    
    switch (serialInput[0])
    {
        case 'A': // Animation
            animNum = serialInput[1]*10+serialInput[2];
            //animation(animNum);
            
            Serial.print("Animation N");
            Serial.println(animNum);
            break;
             
        case 'S': // Servo
            pos = serialInput[2]*100 + serialInput[3]*10 + serialInput[4];

            if ( pos <= 180 && pos >= 0 && ((char) serialInput[1] == 'L' || (char) serialInput[1] == 'R'))
            {
                if ((char) serialInput[1] == 'L')
                    servoLeft.write(pos);
                else if ((char) serialInput[1] == 'R')
                    servoRight.write(pos);
                
                Serial.print("Servo");
                Serial.print((char) serialInput[1]);
                Serial.print(" :");
                Serial.println(pos);
            }
            else
            {
                Serial.println("Servo : Wrong command");
            }
            break;
            
        case 'O': // Offset
            for (int prompt = 1; prompt <= serialPrompt; prompt++)
            {
                if (serialInput[prompt] == ',')
                {
                    pos++;
                    j = 0;
                }
                else if (serialInput[prompt] == '-')
                {
                    minus = -1;
                }
                else
                {
                    command[pos] += (int)serialInput[prompt]*pow(10,j);  
                    j++;
                }
            }
            offsetX(minus*command[1], command[2]);
			Serial.print("Offset :");
			Serial.println (minus*command[1]);
            break;
            
        case 'L': // Led
            for (int prompt = 1; prompt <= serialPrompt; prompt++)
            {
                if (serialInput[prompt] == ',')
                {
                    pos++;
                    j = 0;
                }
                else
                {
                    command[pos] += (int)serialInput[prompt]*pow(10,j);  
                    j++;
                }
            }
            setLed(command[1], command[2], command[3], command[4]);
            Serial.print("SetLed(");
            for (int i = 0; i <4; i++)
            {
                Serial.print(command[i]);
                if (i !=3)
                    Serial.print(",");
                else
                    Serial.println(")");
            }
            
            break;
            
        case 'R': // Refresh
            Serial.println("execute");
            prepare(0,0,DIFF); // Suppose layer[0] set correctly for 2 eyes
            execute();
            break;
        
        case 'B': // BlackOut
            Serial.println("Black out");
            cleanAllBuffers();
            prepare(0,0,CLONE);
            execute();
            break;
            
        case 'V': // Version (usefull for linux autoconnect)
            Serial.println("GobyEyes");
            break;
        
        case 'M': // Mode
            switch(serialInput[1])
            {
                case 1: // auto
                    mode = 1;
                    Serial.println("Mode Auto");
                    break;
                case 2: // pong
                    pongInit();
                    mode = 2;
                    Serial.println("Pong !");
                    break;
                case 3: // manuel
                    mode = 3; 
                    Serial.println("Mode Manuel");
                    break;
            }
            break;
        case 'P': // Pong
            switch(serialInput[1])
            {
                case 1: // Player 01
                    player01_y = serialInput[2];
                    break;
                case 2: // Player 02
                    player02_y = serialInput[2];
                    break;
                case 'B': // Ball Format : PB[xx],[yy]
                    pos = serialInput[2]*10+serialInput[3];
                    ball_x = pos;
                    pos = serialInput[5]*10+serialInput[6];
                    ball_y = pos;
                    break;
            }
            break;
        default:
            Serial.println("Unkwnow command");
    }
    
    // Reset var for next instruction
    for (int i=0; i < 10; i++)
        serialInput[i] = 0;
    serialPrompt=0;
	Timer1.attachInterrupt(matrix);
}