#include <TimerOne.h>
#include <ServoTimer2.h>

#define CLOCK 2
#define MR 3
#define LATCH 4
#define ENABLE 5	

#define SERVO1 13 // Left servo
#define SERVO2 12 // Right servo

#define TEMPS 500

#define DECAL1 8
#define DECAL2 0

#define NB_LAYER 3

// --------- COLORS ---------
#define RED 0
#define BLUE 1
#define GREEN 2

// --------- MODES ---------
#define CLONE 0
#define SYM 1
#define DIFF 2
#define DOUBLE 3

// --------- SPRITES ---------
#define SKULL 1
#define DOLLAR 2
#define EYE 3
#define GIFT 5
#define CHANDEL 6
#define HEART 7
#define SHEART 8


// --------- ANIMATIONS ---------
#define SPACE_IN_VADER 1
#define PACMAN 2
#define STROB 3
#define K2000 4
#define POLICE 6
#define LOVE 7
#define COUNTDOWN 8
#define JACKPOT 9
#define PONG 10

int g_sprite[32][8]; // Sprite on the show ! [Row,Red, Blue, Green x8][Colomn]
unsigned long g_layer[NB_LAYER][8][3];
unsigned long g_buffer_layer[NB_LAYER][8][3];
int g_mode = 0; // Clone by default

ServoTimer2 servoLeft;
ServoTimer2 servoRight;

char serialInput[10];
int serialPrompt = 0;
int animNum = 0;
int mode = 3; // Manuel

// Pong vars
int player01_y = 0;
int player02_y = 0;
int ball_x = 4;
int ball_y = 16;

void setup()
{
	DDRC = 0xff; // pinMode Output for Analog 0-5 (PORTC)(Digital mode)
	pinMode(ENABLE, OUTPUT);
	pinMode(MR, OUTPUT);
	pinMode(CLOCK, OUTPUT);
	pinMode(LATCH, OUTPUT);
	pinMode(6, OUTPUT); // Debug interrupt

	digitalWrite(ENABLE, LOW);
	digitalWrite(MR, HIGH);
    
    servoLeft.attach(SERVO1, 0, 0);
    servoRight.attach(SERVO2, 0, 0);

	Serial.begin(9600);
    
	Timer1.initialize(4000);
	Timer1.attachInterrupt(matrix);
    initRow(); // Init row mask in g_sprite for scanning
	Serial.println("GobyEyes ready");
}

void loop()
{
    if (mode == 2) // Pong
        pongCore();
    else
    {
        if (mode == 1) // Auto
            animNum = random(0, 9);
        if (animNum > 0)
            animation(animNum);
        else
            standby();
    }
}