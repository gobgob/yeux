/**---------------------------------------- **/
/**-------------   SPRITES -------- **/
/**-------------------------------------- **/

void figure(int item)
{
    switch (item)
    {
    case SKULL:
        g_layer[0][0][0] = (unsigned short)( B00000001<< DECAL1 ) | (unsigned short)( B10000000 << DECAL2 );
        g_layer[0][1][0] = (unsigned short)( B00000011<< DECAL1 ) | (unsigned short)( B11000000 << DECAL2 );
        g_layer[0][2][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[0][3][0] = (unsigned short)( B00100111<< DECAL1 ) | (unsigned short)( B11100100 << DECAL2 );
        g_layer[0][4][0] = (unsigned short)( B01100111<< DECAL1 ) | (unsigned short)( B11100110 << DECAL2 );
        g_layer[0][5][0] = (unsigned short)( B00010011<< DECAL1 ) | (unsigned short)( B11001000 << DECAL2 );
        g_layer[0][6][0] = (unsigned short)( B00001011<< DECAL1 ) | (unsigned short)( B11010000 << DECAL2 );
        g_layer[0][7][0] = (unsigned short)( B00000100<< DECAL1 ) | (unsigned short)( B00100000 << DECAL2 );
        g_layer[0][0][1] = (unsigned short)( B00000001<< DECAL1 ) | (unsigned short)( B10000000 << DECAL2 );
        g_layer[0][1][1] = (unsigned short)( B00000011<< DECAL1 ) | (unsigned short)( B11000000 << DECAL2 );
        g_layer[0][2][1] = (unsigned short)( B00000101<< DECAL1 ) | (unsigned short)( B10100000 << DECAL2 );
        g_layer[0][3][1] = (unsigned short)( B00100101<< DECAL1 ) | (unsigned short)( B10100100 << DECAL2 );
        g_layer[0][4][1] = (unsigned short)( B01100111<< DECAL1 ) | (unsigned short)( B11100110 << DECAL2 );
        g_layer[0][5][1] = (unsigned short)( B00010011<< DECAL1 ) | (unsigned short)( B11001000 << DECAL2 );
        g_layer[0][6][1] = (unsigned short)( B00001011<< DECAL1 ) | (unsigned short)( B11010000 << DECAL2 );
        g_layer[0][7][1] = (unsigned short)( B00000100<< DECAL1 ) | (unsigned short)( B00100000 << DECAL2 );
        g_layer[0][0][2] = (unsigned short)( B00000001<< DECAL1 ) | (unsigned short)( B10000000 << DECAL2 );
        g_layer[0][1][2] = (unsigned short)( B00000011<< DECAL1 ) | (unsigned short)( B11000000 << DECAL2 );
        g_layer[0][2][2] = (unsigned short)( B00000101<< DECAL1 ) | (unsigned short)( B10100000 << DECAL2 );
        g_layer[0][3][2] = (unsigned short)( B00100101<< DECAL1 ) | (unsigned short)( B10100100 << DECAL2 );
        g_layer[0][4][2] = (unsigned short)( B01100111<< DECAL1 ) | (unsigned short)( B11100110 << DECAL2 );
        g_layer[0][5][2] = (unsigned short)( B00010011<< DECAL1 ) | (unsigned short)( B11001000 << DECAL2 );
        g_layer[0][6][2] = (unsigned short)( B00001011<< DECAL1 ) | (unsigned short)( B11010000 << DECAL2 );
        g_layer[0][7][2] = (unsigned short)( B00000100<< DECAL1 ) | (unsigned short)( B00100000 << DECAL2 );
        break;
    case DOLLAR:
        g_layer[0][0][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][2] = (unsigned short)( B00101000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][2] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][2] = (unsigned short)( B10101000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][2] = (unsigned short)( B01111100<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][2] = (unsigned short)( B00101010<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][2] = (unsigned short)( B00101010<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][2] = (unsigned short)( B11111100<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][2] = (unsigned short)( B00101000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        break;
    case EYE:
        g_layer[0][0][0] = (unsigned short)( B00111100<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][0] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][0] = (unsigned short)( B11111111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][0] = (unsigned short)( B11100111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][0] = (unsigned short)( B11100111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][0] = (unsigned short)( B11111111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][0] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][0] = (unsigned short)( B00111100<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][1] = (unsigned short)( B00111100<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][1] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][1] = (unsigned short)( B11111111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][1] = (unsigned short)( B11111111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][1] = (unsigned short)( B11111111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][1] = (unsigned short)( B11111111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][1] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][1] = (unsigned short)( B00111100<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][2] = (unsigned short)( B00111100<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][2] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][2] = (unsigned short)( B11111111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][2] = (unsigned short)( B11100111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][2] = (unsigned short)( B11100111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][2] = (unsigned short)( B11111111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][2] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][2] = (unsigned short)( B00111100<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );

        break;
    case GIFT:
        g_layer[0][0][0] = (unsigned short)( B01100110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][0] = (unsigned short)( B00011000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][0] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][0] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][0] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][0] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][0] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][2] = (unsigned short)( B01100110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][2] = (unsigned short)( B00011000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][2] = (unsigned short)( B00011000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][2] = (unsigned short)( B00011000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][2] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][2] = (unsigned short)( B00011000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][2] = (unsigned short)( B00011000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        break;
    case CHANDEL:
        g_layer[0][0][0] = (unsigned short)( B00100000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][0] = (unsigned short)( B01110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][0] = (unsigned short)( B00100000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][0] = (unsigned short)( B01110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][0] = (unsigned short)( B01110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][0] = (unsigned short)( B01110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][0] = (unsigned short)( B01110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][0] = (unsigned short)( B01110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][1] = (unsigned short)( B01110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][1] = (unsigned short)( B01110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][1] = (unsigned short)( B01110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][1] = (unsigned short)( B01110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][1] = (unsigned short)( B01110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][2] = (unsigned short)( B00100000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][2] = (unsigned short)( B01110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][2] = (unsigned short)( B00100000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        break;
    case K2000:
        g_layer[0][0][0] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][0] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][0] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][0] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][0] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][0] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][0] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][0] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        break;
    case HEART:
        g_layer[0][0][0] = (unsigned short)( B00110001<< DECAL1 ) | (unsigned short)( B10000000 << DECAL2 );
        g_layer[0][1][0] = (unsigned short)( B01111011<< DECAL1 ) | (unsigned short)( B11000000 << DECAL2 );
        g_layer[0][2][0] = (unsigned short)( B01111111<< DECAL1 ) | (unsigned short)( B11000000 << DECAL2 );
        g_layer[0][3][0] = (unsigned short)( B01111111<< DECAL1 ) | (unsigned short)( B11000000 << DECAL2 );
        g_layer[0][4][0] = (unsigned short)( B00111111<< DECAL1 ) | (unsigned short)( B10000000 << DECAL2 );
        g_layer[0][5][0] = (unsigned short)( B00011111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][0] = (unsigned short)( B00001110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][0] = (unsigned short)( B00000100<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        break;
    case SHEART:
        g_layer[0][0][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][0] = (unsigned short)( B00110001<< DECAL1 ) | (unsigned short)( B10000000 << DECAL2 );
        g_layer[0][2][0] = (unsigned short)( B01111011<< DECAL1 ) | (unsigned short)( B11000000 << DECAL2 );
        g_layer[0][3][0] = (unsigned short)( B00111111<< DECAL1 ) | (unsigned short)( B10000000 << DECAL2 );
        g_layer[0][4][0] = (unsigned short)( B00011111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][0] = (unsigned short)( B00001110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][0] = (unsigned short)( B00000100<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        break;

    }
}
void pacman(int mode)
{
    if (mode == 1) // Open
    {
        g_layer[0][0][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][0] = (unsigned short)( B00111100<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][0] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][0] = (unsigned short)( B11111000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][0] = (unsigned short)( B11110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][0] = (unsigned short)( B11111000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][0] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][0] = (unsigned short)( B00111100<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][2] = (unsigned short)( B00111100<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][2] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][2] = (unsigned short)( B11111000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][2] = (unsigned short)( B11110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][2] = (unsigned short)( B11111000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][2] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][2] = (unsigned short)( B00111100<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    }
    else
    {
        g_layer[0][0][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][0] = (unsigned short)( B00111100<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][0] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][0] = (unsigned short)( B11111111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][0] = (unsigned short)( B11111111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][0] = (unsigned short)( B11111111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][0] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][0] = (unsigned short)( B00111100<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][2] = (unsigned short)( B00111100<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][2] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][2] = (unsigned short)( B11111111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][4][2] = (unsigned short)( B11111111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][2] = (unsigned short)( B11111111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][2] = (unsigned short)( B01111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][2] = (unsigned short)( B00111100<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    }
}

/**
* Pacman objects -> g_layer[1]
* @param object int
* 1 = bullet
* 2 = blue ghost
**/
void pacmanObject(int object)
{
    switch (object)
    {
    case 1 : // Bullet
        g_layer[1][0][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][1][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][2][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][3][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][4][0] = (unsigned short)( B00010001<< DECAL1 ) | (unsigned short)( B00010001 << DECAL2 );
        g_layer[1][5][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][6][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][7][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][0][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][1][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][2][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][4][1] = (unsigned short)( B00010001<< DECAL1 ) | (unsigned short)( B00010001 << DECAL2 );
        g_layer[1][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][0][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][1][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][2][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][3][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][4][2] = (unsigned short)( B00010001<< DECAL1 ) | (unsigned short)( B00010001 << DECAL2 );
        g_layer[1][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][6][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        break;

    case 2: // Blue ghost
        g_layer[2][0][0] = (unsigned short)( B00011100<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][1][0] = (unsigned short)( B00111110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][2][0] = (unsigned short)( B01111111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][3][0] = (unsigned short)( B01011011<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][4][0] = (unsigned short)( B01011011<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][5][0] = (unsigned short)( B01111111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][6][0] = (unsigned short)( B01111111<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][7][0] = (unsigned short)( B01001001<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][0][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][1][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][2][1] = (unsigned short)( B00110110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][3][1] = (unsigned short)( B00110110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][4][1] = (unsigned short)( B00110110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][0][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][1][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][2][2] = (unsigned short)( B00110110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][3][2] = (unsigned short)( B00010010<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][4][2] = (unsigned short)( B00010010<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][6][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[2][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        break;
    }
}

/**
* Draw a space in vader !
* @param int color (0 to 5)
**/
void spaceInVader(int color)
{
    switch(color)
    {
    case 0:
        g_layer[0][0][0] = (unsigned short)( B00000100 << DECAL1 ) | (unsigned short)( B00010000 << DECAL2 );
        g_layer[0][1][0] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[0][2][0] = (unsigned short)( B00000111 << DECAL1 ) | (unsigned short)( B11110000 << DECAL2 );
        g_layer[0][3][0] = (unsigned short)( B00001101 << DECAL1 ) | (unsigned short)( B11011000 << DECAL2 );
        g_layer[0][4][0] = (unsigned short)( B00011111 << DECAL1 ) | (unsigned short)( B11111100 << DECAL2 );
        g_layer[0][5][0] = (unsigned short)( B00010111 << DECAL1 ) | (unsigned short)( B11110100 << DECAL2 );
        g_layer[0][6][0] = (unsigned short)( B00010100 << DECAL1 ) | (unsigned short)( B00010100 << DECAL2 );
        g_layer[0][7][0] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[0][0][1] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][1] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][1] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][1] = (unsigned short)( B00000010 << DECAL1 ) | (unsigned short)( B00100000 << DECAL2 );
        g_layer[0][4][1] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][1] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][1] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][1] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][2] = (unsigned short)( B00000100 << DECAL1 ) | (unsigned short)( B00010000 << DECAL2 );
        g_layer[0][1][2] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[0][2][2] = (unsigned short)( B00000111 << DECAL1 ) | (unsigned short)( B11110000 << DECAL2 );
        g_layer[0][3][2] = (unsigned short)( B00001101 << DECAL1 ) | (unsigned short)( B11011000 << DECAL2 );
        g_layer[0][4][2] = (unsigned short)( B00011111 << DECAL1 ) | (unsigned short)( B11111100 << DECAL2 );
        g_layer[0][5][2] = (unsigned short)( B00010111 << DECAL1 ) | (unsigned short)( B11110100 << DECAL2 );
        g_layer[0][6][2] = (unsigned short)( B00010100 << DECAL1 ) | (unsigned short)( B00010100 << DECAL2 );
        g_layer[0][7][2] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        break;
    case 1:
        g_layer[0][0][0] = (unsigned short)( B00000100 << DECAL1 ) | (unsigned short)( B00010000 << DECAL2 );
        g_layer[0][1][0] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[0][2][0] = (unsigned short)( B00000111 << DECAL1 ) | (unsigned short)( B11110000 << DECAL2 );
        g_layer[0][3][0] = (unsigned short)( B00001101 << DECAL1 ) | (unsigned short)( B11011000 << DECAL2 );
        g_layer[0][4][0] = (unsigned short)( B00011111 << DECAL1 ) | (unsigned short)( B11111100 << DECAL2 );
        g_layer[0][5][0] = (unsigned short)( B00010111 << DECAL1 ) | (unsigned short)( B11110100 << DECAL2 );
        g_layer[0][6][0] = (unsigned short)( B00010100 << DECAL1 ) | (unsigned short)( B00010100 << DECAL2 );
        g_layer[0][7][0] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[0][0][2] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][2] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][2] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][2] = (unsigned short)( B00000010 << DECAL1 ) | (unsigned short)( B00100000 << DECAL2 );
        g_layer[0][4][2] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][2] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][2] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][2] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][1] = (unsigned short)( B00000100 << DECAL1 ) | (unsigned short)( B00010000 << DECAL2 );
        g_layer[0][1][1] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[0][2][1] = (unsigned short)( B00000111 << DECAL1 ) | (unsigned short)( B11110000 << DECAL2 );
        g_layer[0][3][1] = (unsigned short)( B00001101 << DECAL1 ) | (unsigned short)( B11011000 << DECAL2 );
        g_layer[0][4][1] = (unsigned short)( B00011111 << DECAL1 ) | (unsigned short)( B11111100 << DECAL2 );
        g_layer[0][5][1] = (unsigned short)( B00010111 << DECAL1 ) | (unsigned short)( B11110100 << DECAL2 );
        g_layer[0][6][1] = (unsigned short)( B00010100 << DECAL1 ) | (unsigned short)( B00010100 << DECAL2 );
        g_layer[0][7][1] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        break;
    case 2:
        g_layer[0][0][1] = (unsigned short)( B00000100 << DECAL1 ) | (unsigned short)( B00010000 << DECAL2 );
        g_layer[0][1][1] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[0][2][1] = (unsigned short)( B00000111 << DECAL1 ) | (unsigned short)( B11110000 << DECAL2 );
        g_layer[0][3][1] = (unsigned short)( B00001101 << DECAL1 ) | (unsigned short)( B11011000 << DECAL2 );
        g_layer[0][4][1] = (unsigned short)( B00011111 << DECAL1 ) | (unsigned short)( B11111100 << DECAL2 );
        g_layer[0][5][1] = (unsigned short)( B00010111 << DECAL1 ) | (unsigned short)( B11110100 << DECAL2 );
        g_layer[0][6][1] = (unsigned short)( B00010100 << DECAL1 ) | (unsigned short)( B00010100 << DECAL2 );
        g_layer[0][7][1] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[0][0][0] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][0] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][0] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][0] = (unsigned short)( B00000010 << DECAL1 ) | (unsigned short)( B00100000 << DECAL2 );
        g_layer[0][4][0] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][0] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][0] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][0] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][2] = (unsigned short)( B00000100 << DECAL1 ) | (unsigned short)( B00010000 << DECAL2 );
        g_layer[0][1][2] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[0][2][2] = (unsigned short)( B00000111 << DECAL1 ) | (unsigned short)( B11110000 << DECAL2 );
        g_layer[0][3][2] = (unsigned short)( B00001101 << DECAL1 ) | (unsigned short)( B11011000 << DECAL2 );
        g_layer[0][4][2] = (unsigned short)( B00011111 << DECAL1 ) | (unsigned short)( B11111100 << DECAL2 );
        g_layer[0][5][2] = (unsigned short)( B00010111 << DECAL1 ) | (unsigned short)( B11110100 << DECAL2 );
        g_layer[0][6][2] = (unsigned short)( B00010100 << DECAL1 ) | (unsigned short)( B00010100 << DECAL2 );
        g_layer[0][7][2] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        break;
    case 3:
        g_layer[0][0][1] = (unsigned short)( B00000100 << DECAL1 ) | (unsigned short)( B00010000 << DECAL2 );
        g_layer[0][1][1] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[0][2][1] = (unsigned short)( B00000111 << DECAL1 ) | (unsigned short)( B11110000 << DECAL2 );
        g_layer[0][3][1] = (unsigned short)( B00001101 << DECAL1 ) | (unsigned short)( B11011000 << DECAL2 );
        g_layer[0][4][1] = (unsigned short)( B00011111 << DECAL1 ) | (unsigned short)( B11111100 << DECAL2 );
        g_layer[0][5][1] = (unsigned short)( B00010111 << DECAL1 ) | (unsigned short)( B11110100 << DECAL2 );
        g_layer[0][6][1] = (unsigned short)( B00010100 << DECAL1 ) | (unsigned short)( B00010100 << DECAL2 );
        g_layer[0][7][1] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[0][0][2] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][2] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][2] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][2] = (unsigned short)( B00000010 << DECAL1 ) | (unsigned short)( B00100000 << DECAL2 );
        g_layer[0][4][2] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][2] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][2] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][2] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][0] = (unsigned short)( B00000100 << DECAL1 ) | (unsigned short)( B00010000 << DECAL2 );
        g_layer[0][1][0] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[0][2][0] = (unsigned short)( B00000111 << DECAL1 ) | (unsigned short)( B11110000 << DECAL2 );
        g_layer[0][3][0] = (unsigned short)( B00001101 << DECAL1 ) | (unsigned short)( B11011000 << DECAL2 );
        g_layer[0][4][0] = (unsigned short)( B00011111 << DECAL1 ) | (unsigned short)( B11111100 << DECAL2 );
        g_layer[0][5][0] = (unsigned short)( B00010111 << DECAL1 ) | (unsigned short)( B11110100 << DECAL2 );
        g_layer[0][6][0] = (unsigned short)( B00010100 << DECAL1 ) | (unsigned short)( B00010100 << DECAL2 );
        g_layer[0][7][0] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        break;
    case 4:
        g_layer[0][0][2] = (unsigned short)( B00000100 << DECAL1 ) | (unsigned short)( B00010000 << DECAL2 );
        g_layer[0][1][2] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[0][2][2] = (unsigned short)( B00000111 << DECAL1 ) | (unsigned short)( B11110000 << DECAL2 );
        g_layer[0][3][2] = (unsigned short)( B00001101 << DECAL1 ) | (unsigned short)( B11011000 << DECAL2 );
        g_layer[0][4][2] = (unsigned short)( B00011111 << DECAL1 ) | (unsigned short)( B11111100 << DECAL2 );
        g_layer[0][5][2] = (unsigned short)( B00010111 << DECAL1 ) | (unsigned short)( B11110100 << DECAL2 );
        g_layer[0][6][2] = (unsigned short)( B00010100 << DECAL1 ) | (unsigned short)( B00010100 << DECAL2 );
        g_layer[0][7][2] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[0][0][1] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][1] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][1] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][1] = (unsigned short)( B00000010 << DECAL1 ) | (unsigned short)( B00100000 << DECAL2 );
        g_layer[0][4][1] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][1] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][1] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][1] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][0] = (unsigned short)( B00000100 << DECAL1 ) | (unsigned short)( B00010000 << DECAL2 );
        g_layer[0][1][0] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[0][2][0] = (unsigned short)( B00000111 << DECAL1 ) | (unsigned short)( B11110000 << DECAL2 );
        g_layer[0][3][0] = (unsigned short)( B00001101 << DECAL1 ) | (unsigned short)( B11011000 << DECAL2 );
        g_layer[0][4][0] = (unsigned short)( B00011111 << DECAL1 ) | (unsigned short)( B11111100 << DECAL2 );
        g_layer[0][5][0] = (unsigned short)( B00010111 << DECAL1 ) | (unsigned short)( B11110100 << DECAL2 );
        g_layer[0][6][0] = (unsigned short)( B00010100 << DECAL1 ) | (unsigned short)( B00010100 << DECAL2 );
        g_layer[0][7][0] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        break;
    case 5:
        g_layer[0][0][2] = (unsigned short)( B00000100 << DECAL1 ) | (unsigned short)( B00010000 << DECAL2 );
        g_layer[0][1][2] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[0][2][2] = (unsigned short)( B00000111 << DECAL1 ) | (unsigned short)( B11110000 << DECAL2 );
        g_layer[0][3][2] = (unsigned short)( B00001101 << DECAL1 ) | (unsigned short)( B11011000 << DECAL2 );
        g_layer[0][4][2] = (unsigned short)( B00011111 << DECAL1 ) | (unsigned short)( B11111100 << DECAL2 );
        g_layer[0][5][2] = (unsigned short)( B00010111 << DECAL1 ) | (unsigned short)( B11110100 << DECAL2 );
        g_layer[0][6][2] = (unsigned short)( B00010100 << DECAL1 ) | (unsigned short)( B00010100 << DECAL2 );
        g_layer[0][7][2] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[0][0][0] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][1][0] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][2][0] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][3][0] = (unsigned short)( B00000010 << DECAL1 ) | (unsigned short)( B00100000 << DECAL2 );
        g_layer[0][4][0] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][5][0] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][6][0] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][7][0] = (unsigned short)( B00000000 << DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[0][0][1] = (unsigned short)( B00000100 << DECAL1 ) | (unsigned short)( B00010000 << DECAL2 );
        g_layer[0][1][1] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[0][2][1] = (unsigned short)( B00000111 << DECAL1 ) | (unsigned short)( B11110000 << DECAL2 );
        g_layer[0][3][1] = (unsigned short)( B00001101 << DECAL1 ) | (unsigned short)( B11011000 << DECAL2 );
        g_layer[0][4][1] = (unsigned short)( B00011111 << DECAL1 ) | (unsigned short)( B11111100 << DECAL2 );
        g_layer[0][5][1] = (unsigned short)( B00010111 << DECAL1 ) | (unsigned short)( B11110100 << DECAL2 );
        g_layer[0][6][1] = (unsigned short)( B00010100 << DECAL1 ) | (unsigned short)( B00010100 << DECAL2 );
        g_layer[0][7][1] = (unsigned short)( B00000011 << DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        break;
    }
}

void countDown(int i)
{
    switch(i)
    {
    case 11: //Small 0
        g_layer[1][0][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][1][0] = (unsigned short)( B00000001<< DECAL1 ) | (unsigned short)( B10000000 << DECAL2 );
        g_layer[1][2][0] = (unsigned short)( B00000010<< DECAL1 ) | (unsigned short)( B01000000 << DECAL2 );
        g_layer[1][3][0] = (unsigned short)( B00000010<< DECAL1 ) | (unsigned short)( B01000000 << DECAL2 );
        g_layer[1][4][0] = (unsigned short)( B00000010<< DECAL1 ) | (unsigned short)( B01000000 << DECAL2 );
        g_layer[1][5][0] = (unsigned short)( B00000010<< DECAL1 ) | (unsigned short)( B01000000 << DECAL2 );
        g_layer[1][6][0] = (unsigned short)( B00000001<< DECAL1 ) | (unsigned short)( B10000000 << DECAL2 );
        g_layer[1][7][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][0][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][1][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][2][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][4][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][0][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][1][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][2][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][3][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][4][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][6][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        break;
    case 0:
        g_layer[1][0][0] = (unsigned short)( B00000011<< DECAL1 ) | (unsigned short)( B11000000 << DECAL2 );
        g_layer[1][1][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][2][0] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][3][0] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][4][0] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][5][0] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][6][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][7][0] = (unsigned short)( B00000011<< DECAL1 ) | (unsigned short)( B11000000 << DECAL2 );
        g_layer[1][0][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][1][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][2][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][4][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][0][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][1][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][2][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][3][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][4][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][6][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );


        break;
    case 1:
        g_layer[1][0][0] = (unsigned short)( B00000001<< DECAL1 ) | (unsigned short)( B10000000 << DECAL2 );
        g_layer[1][1][0] = (unsigned short)( B00000011<< DECAL1 ) | (unsigned short)( B10000000 << DECAL2 );
        g_layer[1][2][0] = (unsigned short)( B00000001<< DECAL1 ) | (unsigned short)( B10000000 << DECAL2 );
        g_layer[1][3][0] = (unsigned short)( B00000001<< DECAL1 ) | (unsigned short)( B10000000 << DECAL2 );
        g_layer[1][4][0] = (unsigned short)( B00000001<< DECAL1 ) | (unsigned short)( B10000000 << DECAL2 );
        g_layer[1][5][0] = (unsigned short)( B00000001<< DECAL1 ) | (unsigned short)( B10000000 << DECAL2 );
        g_layer[1][6][0] = (unsigned short)( B00000001<< DECAL1 ) | (unsigned short)( B10000000 << DECAL2 );
        g_layer[1][7][0] = (unsigned short)( B00000001<< DECAL1 ) | (unsigned short)( B10000000 << DECAL2 );
        g_layer[1][0][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][1][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][2][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][4][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][0][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][1][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][2][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][3][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][4][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][6][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );


        break;
    case 2:
        g_layer[1][0][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][1][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][2][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][3][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][4][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][5][0] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][6][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][7][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][0][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][1][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][2][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][4][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][0][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][1][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][2][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][3][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][4][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][6][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );

        break;
    case 3:
        g_layer[1][0][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][1][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][2][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][3][0] = (unsigned short)( B00000011<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][4][0] = (unsigned short)( B00000011<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][5][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][6][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][7][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][0][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][1][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][2][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][4][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][0][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][1][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][2][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][3][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][4][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][6][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );

        break;
    case 4:
        g_layer[1][0][0] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][1][0] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][2][0] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][3][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][4][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][5][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][6][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][7][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][0][1] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][1][1] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][2][1] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][3][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][4][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][0][2] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][1][2] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][2][2] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][3][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][4][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][6][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );

        break;
    case 5:
        g_layer[1][0][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][1][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][2][0] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][3][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][4][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][5][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][6][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][7][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][0][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][1][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][2][1] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][3][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][4][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][6][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][7][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][0][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][1][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][2][2] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][3][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][4][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][6][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][7][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );

        break;
    case 6:
        g_layer[1][0][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][1][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][2][0] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][3][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][4][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][5][0] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][6][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][7][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][0][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][1][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][2][1] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][3][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][4][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][5][1] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][6][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][7][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][0][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][1][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][2][2] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[1][3][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][4][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][5][2] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][6][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][7][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );

        break;
    case 7:
        g_layer[1][0][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][1][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][2][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][3][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][4][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][5][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][6][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][7][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][0][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][1][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][2][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][4][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][0][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][1][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][2][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][3][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][4][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][6][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );

        break;
    case 8:
        g_layer[1][0][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][1][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][2][0] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][3][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][4][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][5][0] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][6][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][7][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][0][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][1][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][2][1] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][3][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][4][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][5][1] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][6][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][7][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][0][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][1][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][2][2] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][3][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][4][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][5][2] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][6][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][7][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );

        break;
    case 9:
        g_layer[1][0][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][1][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][2][0] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][3][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][4][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][5][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][6][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][7][0] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][0][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][1][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][2][1] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][3][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][4][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][6][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][7][1] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][0][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][1][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][2][2] = (unsigned short)( B00000110<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][3][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][4][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B01100000 << DECAL2 );
        g_layer[1][6][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        g_layer[1][7][2] = (unsigned short)( B00000111<< DECAL1 ) | (unsigned short)( B11100000 << DECAL2 );
        break;
    case 10:
        g_layer[1][0][0] = (unsigned short)( B00001100<< DECAL1 ) | (unsigned short)( B11110000 << DECAL2 );
        g_layer[1][1][0] = (unsigned short)( B00011101<< DECAL1 ) | (unsigned short)( B11111000 << DECAL2 );
        g_layer[1][2][0] = (unsigned short)( B00001101<< DECAL1 ) | (unsigned short)( B10011000 << DECAL2 );
        g_layer[1][3][0] = (unsigned short)( B00001101<< DECAL1 ) | (unsigned short)( B10011000 << DECAL2 );
        g_layer[1][4][0] = (unsigned short)( B00001101<< DECAL1 ) | (unsigned short)( B10011000 << DECAL2 );
        g_layer[1][5][0] = (unsigned short)( B00001101<< DECAL1 ) | (unsigned short)( B10011000 << DECAL2 );
        g_layer[1][6][0] = (unsigned short)( B00001101<< DECAL1 ) | (unsigned short)( B11111000 << DECAL2 );
        g_layer[1][7][0] = (unsigned short)( B00001100<< DECAL1 ) | (unsigned short)( B11110000 << DECAL2 );
        g_layer[1][0][1] = (unsigned short)( B00001100<< DECAL1 ) | (unsigned short)( B11110000 << DECAL2 );
        g_layer[1][1][1] = (unsigned short)( B00011101<< DECAL1 ) | (unsigned short)( B11111000 << DECAL2 );
        g_layer[1][2][1] = (unsigned short)( B00001101<< DECAL1 ) | (unsigned short)( B10011000 << DECAL2 );
        g_layer[1][3][1] = (unsigned short)( B00001101<< DECAL1 ) | (unsigned short)( B10011000 << DECAL2 );
        g_layer[1][4][1] = (unsigned short)( B00001101<< DECAL1 ) | (unsigned short)( B10011000 << DECAL2 );
        g_layer[1][5][1] = (unsigned short)( B00001101<< DECAL1 ) | (unsigned short)( B10011000 << DECAL2 );
        g_layer[1][6][1] = (unsigned short)( B00001101<< DECAL1 ) | (unsigned short)( B11111000 << DECAL2 );
        g_layer[1][7][1] = (unsigned short)( B00001100<< DECAL1 ) | (unsigned short)( B11110000 << DECAL2 );
        g_layer[1][0][2] = (unsigned short)( B00001100<< DECAL1 ) | (unsigned short)( B11110000 << DECAL2 );
        g_layer[1][1][2] = (unsigned short)( B00011101<< DECAL1 ) | (unsigned short)( B11111000 << DECAL2 );
        g_layer[1][2][2] = (unsigned short)( B00001101<< DECAL1 ) | (unsigned short)( B10011000 << DECAL2 );
        g_layer[1][3][2] = (unsigned short)( B00001101<< DECAL1 ) | (unsigned short)( B10011000 << DECAL2 );
        g_layer[1][4][2] = (unsigned short)( B00001101<< DECAL1 ) | (unsigned short)( B10011000 << DECAL2 );
        g_layer[1][5][2] = (unsigned short)( B00001101<< DECAL1 ) | (unsigned short)( B10011000 << DECAL2 );
        g_layer[1][6][2] = (unsigned short)( B00001101<< DECAL1 ) | (unsigned short)( B11111000 << DECAL2 );
        g_layer[1][7][2] = (unsigned short)( B00001100<< DECAL1 ) | (unsigned short)( B11110000 << DECAL2 );
        break;
    }  
}

void jackpot(int i, int layer)
{
    switch(i)
    {
    case 1: // Seven
        g_layer[layer][0][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][1][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][2][0] = (unsigned short)( B01110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][3][0] = (unsigned short)( B00010000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][4][0] = (unsigned short)( B00100000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][5][0] = (unsigned short)( B00100000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][6][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][7][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][0][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][1][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][2][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][4][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][0][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][1][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][2][2] = (unsigned short)( B01110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][3][2] = (unsigned short)( B00010000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][4][2] = (unsigned short)( B00100000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][5][2] = (unsigned short)( B00100000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][6][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        break;
    case 2: // Cherry
        g_layer[layer][0][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][1][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][2][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][3][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][4][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][5][0] = (unsigned short)( B01010000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][6][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][7][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][0][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][1][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][2][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][4][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][0][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][1][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][2][2] = (unsigned short)( B00010000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][3][2] = (unsigned short)( B00100000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][4][2] = (unsigned short)( B01010000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][6][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        break;
    case 3: // Heart
        g_layer[layer][0][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][1][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][2][0] = (unsigned short)( B01010000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][3][0] = (unsigned short)( B11111000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][4][0] = (unsigned short)( B01110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][5][0] = (unsigned short)( B00100000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][6][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][7][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][0][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][1][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][2][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][4][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][0][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][1][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][2][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][3][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][4][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][6][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        break;
    case 4: //Lemon
        g_layer[layer][0][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][1][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][2][0] = (unsigned short)( B01110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][3][0] = (unsigned short)( B11111000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][4][0] = (unsigned short)( B01110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][5][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][6][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][7][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][0][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][1][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][2][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][4][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][0][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][1][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][2][2] = (unsigned short)( B01110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][3][2] = (unsigned short)( B11111000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][4][2] = (unsigned short)( B01110000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][6][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        g_layer[layer][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
        break;
    }
    
}