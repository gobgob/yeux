/**
 PONG GAME - 2 PLAYERS
**/

void pongInit()
{
    cleanAllLayers();
    cleanAllBuffers();
    
    // Set ball
    g_layer[0][0][0] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][1][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][2][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][3][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][4][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][5][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][6][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][7][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][0][1] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][1][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][2][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][4][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][0][2] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][1][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][2][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][3][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][4][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][6][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[0][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );

    
    // Set players 1
    g_layer[1][0][0] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][1][0] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][2][0] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][3][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][4][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][5][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][6][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][7][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][0][1] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][1][1] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][2][1] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][4][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][0][2] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][1][2] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][2][2] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][3][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][4][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][6][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[1][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );

    g_layer[1][2][2] = B10000000 << 8;
    
    // Set players 2
    g_layer[2][0][0] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][1][0] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][2][0] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][3][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][4][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][5][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][6][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][7][0] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][0][1] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][1][1] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][2][1] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][3][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][4][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][5][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][6][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][7][1] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][0][2] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][1][2] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][2][2] = (unsigned short)( B10000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][3][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][4][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][5][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][6][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );
    g_layer[2][7][2] = (unsigned short)( B00000000<< DECAL1 ) | (unsigned short)( B00000000 << DECAL2 );

    
}

void pongCore()
{
    // Display
    prepare(1,0,DIFF);
    offsetY(-player01_y,1);
    
    prepare(2,31,DIFF);
    offsetY(-player02_y,2);
    
    prepare(0,ball_x, DIFF);
    offsetY(-ball_y, 0);
    
    execute();
}