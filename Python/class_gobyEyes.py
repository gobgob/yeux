#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
GobyEyes Library

This library is for communicate with gobyEyes via serial

Author : Fabien0102
Last update : April 2013
"""
import serial

import os
import time

BAUD_RATE = 9600

class GobyEyes():
    def __init__(self, port ='auto'):
        if (port == 'auto'):
            self._ser = None
            self._serOpen = False
            self._serPort = None
            self._auto_connect() # Auto connect serial port
        else:
            self._ser = serial.Serial(port, BAUD_RATE)
            self._serOpen = True
            self._serPort = port
            
        self._debug = False
        
        # Mode manual
        self._run("M3")
            
    """
    ------------------
    --- ANIMATIONS ---
    ------------------
    """
    def space_in_vader(self):
        self._run("A01")    
    def pacman(self):
        self._run("A02")
    def strob(self):
        self._run("A03")
    def K2000(self):
        self._run("A04")
    def gift(self):
        self._run("A05")
    def police(self):
        self._run("A06")
    def love(self):
        self._run("A07")
    def count_down(self):
        self._run("A08")
    def jackpot(self):
        self._run("A09") 
    def rotation_left(self, angle):
        value = str(angle)
        self.run("SL"+value.rjust(3,'0'))
    def rotation_right(self, angle):
        value = str(abs(180-angle))
        print value
        # self.run("SR"+value.rjust(3,'0'))
    
    """
    ------------
    --- CORE ---
    ------------
    """
    def _help(self):
        print "------------"
        print "Animations :"
        print "------------"
        for anim in dir(self):
            if "_" not in anim:
                print anim
                
        print "------------"
        print "debug (on|off)"
    def _set_debug(self, value):
        self._debug = value
    def _auto_connect(self):
        for port in self._list_serial_ports():
            try:
                ser = serial.Serial(port, baudrate= BAUD_RATE, timeout=2)
                ser.write("V\n")
                result = ser.readline()
                if "GobyEyes" in result:
                        print "Connect Successful! Connected on port:",port
                        self._ser = ser
                        self._ser.flush()
                        self._serOpen = True
                        self._serPort = port
                        break
            except:
                    pass
        if self._serOpen == False:
            print "Connection Failed :("
    def _list_serial_ports(self):
        # Windows
        if os.name == 'nt':
            # Scan for available ports.
            available = []
            for i in range(256):
                try:
                    ser = serial.Serial(i)
                    available.append('COM'+str(i + 1))
                    ser.close()
                except serial.SerialException:
                    pass
            return available
        else:
            # Mac / Linux
            # return [port[0] for port in list_ports.comports()]
            return 0
    def _run(self, command):
        if self._serOpen == False:
            print "Connection Failed :("
        else:
            # Send command into serial
            self._ser.write(str(command)+"\n")
            
            # Print respond of GobyEyes (Debug only)
            if self._ser.readable() and self._debug:
                read = self._ser.readline()
                if len(read) == 0:
                    print "No respond of gobyEyes :( "
                else:
                    print "[%s] %s"%(self._serPort, str(read))
        
if __name__ == '__main__':
    # CLI Interface
    quit = False
    print "GobyEyes 1.0.0"
    print "Type \"help\" for listing animations availables"
    
    gobyeyes = GobyEyes()
    while not quit:
        cmd = raw_input(">>> ")
        
        if cmd in ["quit", "q", "exit", ":q"]:
            quit = True
        elif cmd in dir(gobyeyes): #cmd is in defines functions
            eval("gobyeyes."+cmd+"()") #exec cmd as function :)
        elif cmd == "help":
            gobyeyes._help()
        elif cmd == "debug on":
            gobyeyes._set_debug(True)
        elif cmd == "debug off":
            gobyeyes._set_debug(False)
        else:
            gobyeyes._run(cmd)
            
    print "Good bye :)"
