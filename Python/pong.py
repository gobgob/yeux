#!/usr/bin/python
# -*- coding: utf-8 -*-

""" PONG GAME """

import sys
from time import sleep
from class_gobyEyes import *
from pygame import *


class Pong():
    def __init__(self):
        self.gobyEyes = GobyEyes()
        self.gobyEyes._run("M2")
        
        self.player01_y = 0
        self.player02_y = 0
        
        self.player01_pts = 0
        self.player02_pts = 0
        
        self.ball_vitesse_y = -1
        self.ball_vitesse_x = 1
        self.ball_x = 1
        self.ball_y = 1
        
        init()

        self.screen = display.set_mode((1920, 1080), RESIZABLE)
        display.set_caption('Pong')
        
        # Score
        draw.rect(self.screen, (0,0,0), Rect(0, 0, 2000, 2000))
        myfont = font.SysFont(None, 500)
        score = myfont.render("0 - 0", 1, (255,255,255))
        self.screen.blit(score, (500, 300))
        display.flip()
        
    def game(self):
       
        while True:
            # Events
            for e in event.get():
                if e.type == KEYDOWN and e.key == K_r:
                    self.move(1,"up")
                    print "P01 Up"
                if e.type == KEYDOWN and e.key == K_f:
                    self.move(1,"down")
                    print "P01 Down"
                if e.type == KEYDOWN and e.key == K_UP:
                    self.move(2,"up")
                    print "P02 Up"
                if e.type == KEYDOWN and e.key == K_DOWN:
                    self.move(2,"down")
                    print "P02 Down"
                if e.type == KEYDOWN and e.key == K_SPACE:
                    self.player01_pts = 0
                    self.player02_pts = 0
                    self.resetMatch()
                    
                if e.type == QUIT:
                    quit()
                    sys.exit()
            # Core
            self.hitTest()
            self.ball_x += self.ball_vitesse_x
            self.ball_y += self.ball_vitesse_y
            self.gobyEyes._run("PB"+str(self.ball_x).rjust(2,'0')+","+str(self.ball_y).rjust(2,'0'))
            sleep(.1)
            # print "PB"+str(self.ball_x).rjust(2,'0')+","+str(self.ball_y).rjust(2,'0')

    def move(self, player, sens):
        if player == 1:
            if sens == "up" and self.player01_y > 0:
                self.player01_y -= 1
            elif sens == "down" and self.player01_y < 5:
                self.player01_y += 1
        elif player == 2:
            if sens == "up" and self.player02_y > 0:
                self.player02_y -= 1
            elif sens == "down" and self.player02_y < 5:
                self.player02_y += 1
                
        self.gobyEyes._run("P"+str(player)+str(eval("self.player0"+str(player)+"_y")))
    
    def hitTest(self):
        # map
        if self.ball_y + self.ball_vitesse_y < 0 :
            self.ball_vitesse_y = 1
        elif self.ball_y + self.ball_vitesse_y > 7 :
            self.ball_vitesse_y = -1
        
        # player01
        if self.ball_x == 1 and self.ball_vitesse_x == -1:
            if self.ball_y + self.ball_vitesse_y == self.player01_y - 1 \
            or self.ball_y + self.ball_vitesse_y == self.player01_y     \
            or self.ball_y + self.ball_vitesse_y == self.player01_y + 1 \
            or self.ball_y + self.ball_vitesse_y == self.player01_y + 2 \
            or self.ball_y + self.ball_vitesse_y == self.player01_y + 3:
                self.ball_vitesse_x = 1
            else :
                self.player01_pts += 1
                self.resetMatch()
        else:
            pass
               
        # player02
        if self.ball_x == 30 and self.ball_vitesse_x == 1:
            if self.ball_y + self.ball_vitesse_y == self.player02_y - 1 \
            or self.ball_y + self.ball_vitesse_y == self.player02_y     \
            or self.ball_y + self.ball_vitesse_y == self.player02_y + 1 \
            or self.ball_y + self.ball_vitesse_y == self.player02_y + 2 \
            or self.ball_y + self.ball_vitesse_y == self.player02_y + 3:
                self.ball_vitesse_x = -1
            else :
                self.player02_pts += 1
                self.resetMatch()
        else:
            pass
    
    def resetMatch(self):
        print "Score :"+ str(self.player01_pts) + " - " + str(self.player02_pts)
        
        # Score
        draw.rect(self.screen, (0,0,0), Rect(0, 0, 2000, 2000))
        myfont = font.SysFont(None, 500)
        score = myfont.render(str(self.player01_pts) + " - " + str(self.player02_pts), 1, (255,255,255))
        self.screen.blit(score, (500, 300))
        display.flip()
        
        self.ball_x = 16
        self.ball_y = 3
        self.ball_vitesse_y *= -1
                

if __name__ == '__main__':
    pong = Pong()
    pong.game()                