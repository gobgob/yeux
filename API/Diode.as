﻿package  {
	
	// import flash.display.MovieClip;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;
	
	public class Diode extends MovieClip {
		
		protected var _value:int;
		protected var circle:Shape = new Shape();
		protected var myColor:ColorTransform = new ColorTransform();
		
		public function Diode() {
			// Default values
			value = 0; //black
			
			circle.graphics.beginFill(0x000000, 1); // Fill the circle with the color black
			circle.graphics.drawCircle(0, 0, 20); // Draw the circle, assigning it a x position, y position, raidius.
			circle.graphics.endFill(); // End the filling of the circle
			
			addChild(circle);
			addEventListener(MouseEvent.MOUSE_DOWN, mouseClick);
			addEventListener(MouseEvent.MOUSE_OVER, mouseOver);
		}
		
		public function mouseOver(e:MouseEvent):void
		{
			if(e.ctrlKey){
				// Switch off if ctrl key is down
				myColor.color = 0x000000;
				value = 0;
				circle.transform.colorTransform = myColor;
			} else {
				if(e.buttonDown){
					switch (yeux_robot.color) {
						case 0:
							// Black
							myColor.color = 0x000000 ;
							break;
						case 1:
							// Blue
							myColor.color = 0x0000ff ;
							break;
						case 2:
							// Green
							myColor.color = 0x00ff00 ;
							break;
						case 3:
							// Cyan
							myColor.color = 0x00ffff ;
							break;
						case 4:
							// Red
							myColor.color = 0xff0000 ;
							break;
						case 5:
							// Purple
							myColor.color = 0xff00ff ;
							break;
						case 6:
							// Yellow
							myColor.color = 0xffff00 ;
							break;
						case 7:
							// White
							myColor.color = 0xffffff ;
							break;
					}
					
					value = yeux_robot.color;
					circle.transform.colorTransform = myColor;
				}
			}
		}
		
		public function mouseClick(e:MouseEvent):void
		{
			if(e.ctrlKey){
				// Switch off if ctrl key is down
				myColor.color = 0x000000;
				value = 0;
				circle.transform.colorTransform = myColor;
			} else {
				switch (yeux_robot.color) {
					case 0:
						// Black
						myColor.color = 0x000000 ;
						break;
					case 1:
						// Blue
						myColor.color = 0x0000ff ;
						break;
					case 2:
						// Green
						myColor.color = 0x00ff00 ;
						break;
					case 3:
						// Cyan
						myColor.color = 0x00ffff ;
						break;
					case 4:
						// Red
						myColor.color = 0xff0000 ;
						break;
					case 5:
						// Purple
						myColor.color = 0xff00ff ;
						break;
					case 6:
						// Yellow
						myColor.color = 0xffff00 ;
						break;
					case 7:
						// White
						myColor.color = 0xffffff ;
						break;
				}
					value = yeux_robot.color;
					circle.transform.colorTransform = myColor;
			}
		}
		
		public function clear():void
		{
			myColor.color = 0x000000;
			value = 0;
			circle.transform.colorTransform = myColor;
			
		}
		
		public function set value(value):void{
			_value = value;
		}
		
		public function get value():int{
			return _value;
		}
	}
	
}
