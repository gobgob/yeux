﻿package
{
	import flash.desktop.Clipboard;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.system.System;
	
	public class yeux_robot extends Sprite
	{
		public static var color:int = 0; // Color for painting
		public static var colorPicker:ColorPicker = new ColorPicker();
		public var aide:Aide = new Aide();
		public var layer:int = 1; // Number of the layer for export
		
		public function yeux_robot()
		{	
			//Left eye
			for(var col:int=0; col<16 ; col++)
			{
				for(var row:int=0; row<8; row++)
				{
					var diode000:Diode = new Diode();
					diode000.name = "diode_"+row+"_"+col;
					diode000.x = 45*col +300;
					diode000.y = 45*row +200;
					addChild(diode000);
				}
			}
			
			// Color picker
			colorPicker.x = 30;
			colorPicker.y = 30;
			addChild(colorPicker);
			
			// Event Listener
			stage.addEventListener(KeyboardEvent.KEY_DOWN, export);
			
			// Help menu (F1)
			aide.x = stage.width/2;
			aide.y = stage.height/2;
		}
        
		public function export (e:KeyboardEvent)
		{
			trace (e.keyCode);
			var print:String = new String();
			switch(e.keyCode)
			{
				case 32 : //Touche espace -> print result
					
					print = "";
					for (var color:int= 0; color<3; color++)
					{
						for(var row:int=0; row<8; row++)
						{
                            print += "g_layer["+layer+"]["+row+"]["+color+"] = (unsigned short)( B";

							for(var col:int=0; col<16 ; col++)
							{
								if (col == 8) 
								{
									print += "<< DECAL1 ) | (unsigned short)( B";
								}
								// print += Diode(getChildByName("diode_"+row+"_"+col)).value;
								switch (color)
								{
									case 0: // RED
										switch (Diode(getChildByName("diode_"+row+"_"+col)).value)
										{
											case 4:
											case 5:
											case 6:
											case 7:
												print += "1";
												break;
											default:
												print += "0";
												break;
										}
										break;
									case 1: // BLUE
										switch (Diode(getChildByName("diode_"+row+"_"+col)).value)
										{
											case 1:
											case 3:
											case 5:
											case 7:
												print += "1";
												break;
											default:
												print += "0";
												break;
										}
										break;
									case 2: // GREEN
										switch (Diode(getChildByName("diode_"+row+"_"+col)).value)
										{
											case 2:
											case 3:
											case 6:
											case 7:
												print += "1";
												break;
											default:
												print += "0";
												break;
												
										}
										break;
								}
							}
                            print += " << DECAL2 );\n";
						}
					}
					
					//trace(diode(getChildByName("diode_3_1")).value);
					trace(print);
					System.setClipboard(print);
				break;
				
				case 46 : // Touche suppr -> Clear display
					for(col=0; col<16 ; col++)
					{
						for(row=0; row<8; row++)
						{
							Diode(getChildByName("diode_"+row+"_"+col)).clear();
						}
					}
					break;
				
				case 112 : // F1 -> Aide
				case 27 : // Echap
						if (stage.contains(aide))
							removeChild(aide);
						else
							addChild(aide);
					break;
				
				case 38 : // Up
					if (yeux_robot.color > 0){
						yeux_robot.color--;
						yeux_robot.colorPicker.refreshColor();
					}
					break;
				
				case 40 : // Down
					if (yeux_robot.color < 7){
						yeux_robot.color++;
						yeux_robot.colorPicker.refreshColor();
					}
					break;
				
				case 49 : // 1 -> black
					yeux_robot.color = 0;
					yeux_robot.colorPicker.refreshColor();
					layer = 1;
					break;
				
				case 50 : // 2 -> blue
					yeux_robot.color = 1;
					yeux_robot.colorPicker.refreshColor();
					layer = 2;
					break;
				
				case 51 : // 3 -> green
					yeux_robot.color = 2;
					yeux_robot.colorPicker.refreshColor();
					layer = 3;
					break;
				
				case 52 : // 4 -> cyan
					yeux_robot.color = 3;
					yeux_robot.colorPicker.refreshColor();
					layer = 4;
					break;
				
				case 53 : // 5 -> red
					yeux_robot.color = 4;
					yeux_robot.colorPicker.refreshColor();
					break;
				
				case 54 : // 6 -> pink
					yeux_robot.color = 5;
					yeux_robot.colorPicker.refreshColor();
					break;
				
				case 55 : // 7 -> yellow
					yeux_robot.color = 6;
					yeux_robot.colorPicker.refreshColor();
					break;
				
				case 56 : // 8 -> white
					yeux_robot.color = 7;
					yeux_robot.colorPicker.refreshColor();
					break;
				
			}
		}
	}
}