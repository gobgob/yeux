package
{
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;

	public class ColorPicker extends MovieClip
	{
		protected var myColor:ColorTransform = new ColorTransform();
		protected var rect:Shape = new Shape();
		
		public function ColorPicker()
		{
			// Visualisateur de la couleur courante
			rect.graphics.beginFill(0x000000, 1);
			rect.graphics.drawRect(0,0,100,100);
			rect.graphics.endFill();
			
			addChild(rect);
			
			// Color Picker
			var colors:Array = new Array(
				"0x000000",
				"0x0000ff",
				"0x00ff00",
				"0x00ffff",
				"0xff0000",
				"0xff00ff",
				"0xffff00",
				"0xffffff");
			for (var i:* in colors) {
				var color:Color = new Color(colors[i],i);
				color.x = 30;
				color.y = 60*i + 120;
				addChild(color);
			}
		}
				
		public function refreshColor():void
		{
			switch (yeux_robot.color) {
				case 0:
					// Black
					myColor.color = 0x000000 ;
					break;
				case 1:
					// Blue
					myColor.color = 0x0000ff ;
					break;
				case 2:
					// Green
					myColor.color = 0x00ff00 ;
					break;
				case 3:
					// Cyan
					myColor.color = 0x00ffff ;
					break;
				case 4:
					// Red
					myColor.color = 0xff0000 ;
					break;
				case 5:
					// Purple
					myColor.color = 0xff00ff ;
					break;
				case 6:
					// Yellow
					myColor.color = 0xffff00 ;
					break;
				case 7:
					// White
					myColor.color = 0xffffff ;
					break;
			}
			
			rect.transform.colorTransform = myColor;
		}
	}
}