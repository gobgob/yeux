package
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;

	public class Color extends MovieClip
	{
		protected var _value:int;
		
		public function Color(colorInput:uint, valueInput:int)
		{
			value = valueInput;
			var mc_color:MovieClip = new MovieClip();
			mc_color.graphics.beginFill(colorInput,1);
			mc_color.graphics.drawRect(0,0,50,50);
			mc_color.graphics.endFill();
			addChild(mc_color);
			
			mc_color.addEventListener(MouseEvent.CLICK, changeColor);
		}

		public function get value():int
		{
			return _value;
		}

		public function set value(value:int):void
		{
			_value = value;
		}

		public function changeColor(e:MouseEvent):void
		{
			// trace(this.value);
			yeux_robot.color = this.value;
			
			yeux_robot.colorPicker.refreshColor();
		}
	}
}